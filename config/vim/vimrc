"--------------------------- Use XDG folders ----------------------------------"
set undodir=$XDG_CACHE_HOME/vim/undo
set directory=$XDG_CACHE_HOME/vim/swap
set backupdir=$XDG_CACHE_HOME/vim/backup
set viminfo+='1000,n$XDG_CACHE_HOME/vim/viminfo
set runtimepath=$XDG_CONFIG_HOME/vim,$VIMRUNTIME,$XDG_CONFIG_HOME/vim/after
"------------------------------------------------------------------------------"

call plug#begin()

" Add support for surround objects. e.g.: `cs'"`
Plug 'tpope/vim-surround'
" Add support to comment verb. e.g.: `gcc` to comment out a line
Plug 'tpope/vim-commentary'
" Add `.` support to some. e.g.: vim-surround
Plug 'tpope/vim-repeat'
" Heuristically set buffer options like shiftwidth and expandtab
Plug 'tpope/vim-sleuth'
" Add Git gutter to vim
Plug 'airblade/vim-gitgutter'
" Add editorconfig support
Plug 'editorconfig/editorconfig-vim'

call plug#end()

syntax on

" set textwidth
set textwidth=74

" The following format options might not seem necessary because vim
" default ftplugin (File Type) might show different options for different
" file types, we want to force the following  
"
" The following definitions in comments are quoted from `:h fo-table`
"
" t	Auto-wrap text using textwidth
set formatoptions+=t
" l     Long lines are not broken in insert mode
"       When a line was longer than 'textwidth' when the insert command
"       started, Vim does not automatically format it.
set formatoptions-=l

" show relative line numbers with current line
set number
set relativenumber

" show command in status bar
set showcmd

" always show status bar
set laststatus=2

" show menu with file wildcard matches
set wildmenu

" highlight search
set hlsearch

" don't go to begin of file after search hits bottom
set nowrapscan

" Change Spell from highlight color to underline
hi clear SpellBad
hi SpellBad cterm=underline

" Remove editor config ugly ColorColumn
let g:EditorConfig_max_line_indicator="none"

" [Avoiding the "Hit ENTER to continue" prompts][1]
" [1]: https://vim.fandom.com/wiki/Avoiding_the_%22Hit_ENTER_to_continue%22_prompts
command! -nargs=+ Silent
\   execute 'silent ! <args>'
\ | redraw!

" Seriously hacked in order to work with our Silent command
map <silent> <Space> :w<Enter>:Silent !<Enter>
